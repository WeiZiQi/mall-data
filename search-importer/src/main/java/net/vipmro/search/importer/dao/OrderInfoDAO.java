package net.vipmro.search.importer.dao;

import java.util.List;
import java.util.Map;

/**
 * @author fengxiangyang
 * @date 2018/9/7
 */

public interface OrderInfoDAO {

    List<Map<String, Object>> findAll();

}
