package net.vipmro.search.importer.common.listener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.List;

/**
 * 消费者
 * @author fengxiangyang
 * @date 2018/12/18
 */
@Component
public class KafkaConsumer {
    private static final Logger logger = LoggerFactory.getLogger(KafkaConsumer.class);


    /**
     * 监听activity-info主题,有消息就读取
     */
    @KafkaListener(topics = "activity-info")
    public void onMessage(List<String> records) {
        if(records!= null){
            logger.info("kafka消息：records={}",records.size());
        }
    }
}