package net.vipmro.search.importer.dao;

import net.vipmro.search.dto.entity.SellerGoodsInfo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author fengxiangyang
 * @date 2018/12/4
 */
public interface SellerGoodsInfoDAO {

    /**
     * 分页查询
     * @param id
     * @param pageSize
     * @return
     */
    List<SellerGoodsInfo> findPage(@Param("id") Long id, @Param("pageSize") Integer pageSize);

    /**
     * 根据多个id获取商品信息列表
     * @param ids
     * @return
     */
    List<SellerGoodsInfo> findByIds(@Param("ids") String ids);

    /**
     * 根据多个goodsId获取商品信息列表
     * @param goodsIds
     * @return
     */
    List<String> findByGoodsIds(@Param("goodsIds") String goodsIds);
}
