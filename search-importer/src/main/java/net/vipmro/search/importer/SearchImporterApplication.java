package net.vipmro.search.importer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author fengxiangyang
 * @date 2018/12/26
 */
@SpringBootApplication
public class SearchImporterApplication {

    public static void main(String[] args) {
        SpringApplication.run(SearchImporterApplication.class, args);
    }
}
