package net.vipmro.search.importer.dao;


import net.vipmro.search.dto.entity.Category;

import java.util.List;

/**
 * @author fengxiangyang
 */
public interface CategoryDAO {

    List<Category> findAll();
}
