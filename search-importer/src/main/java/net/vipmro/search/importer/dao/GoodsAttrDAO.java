package net.vipmro.search.importer.dao;


import net.vipmro.search.dto.entity.GoodsAttr;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author fengxiangyang
 */
public interface GoodsAttrDAO {

    List<GoodsAttr> findByGoodsId(@Param("goodsId")Long goodsId);

    List<GoodsAttr> findByGoodsIds(@Param("goodsIds")String goodsIds);
}
