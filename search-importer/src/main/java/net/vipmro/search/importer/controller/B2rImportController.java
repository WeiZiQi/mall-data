package net.vipmro.search.importer.controller;

import com.sunny.core.model.ResultInfo;
import net.vipmro.search.importer.service.b2r.SellerGoodsInfoService;
import net.vipmro.search.dto.pojo.DataPacket;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import static com.sunny.core.constants.ResultConstant.CODE_SUCCESS;

/**
 * @author fengxiangyang
 * @date 2018/12/3
 */
@RestController
@RequestMapping("b2r")
public class B2rImportController {

    @Autowired
    private SellerGoodsInfoService sellerGoodsInfoService;

    /**
     * 销售商品全量导入
     * @return
     */
    @RequestMapping("reindex")
    public ResultInfo reindex(){
        new Thread(() -> sellerGoodsInfoService.reindex()).start();
        return new ResultInfo(CODE_SUCCESS,"执行成功");
    }

    /**
     * 销售商品增量导入
     * @return
     */
    @RequestMapping(value = "update", method = RequestMethod.POST)
    public ResultInfo update(@RequestBody DataPacket dataPacket){
        sellerGoodsInfoService.update(dataPacket);
        return new ResultInfo(CODE_SUCCESS,"更新成功");
    }

}
