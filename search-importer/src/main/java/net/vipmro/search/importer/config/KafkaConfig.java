package net.vipmro.search.importer.config;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory;
import org.springframework.kafka.config.KafkaListenerContainerFactory;
import org.springframework.kafka.core.ConsumerFactory;
import org.springframework.kafka.core.DefaultKafkaConsumerFactory;
import org.springframework.kafka.listener.ConcurrentMessageListenerContainer;

import java.util.HashMap;
import java.util.Map;

/**
 * @author fengxiangyang
 * @date 2018/12/26
 */
@Configuration
@EnableKafka
public class KafkaConfig {
    @Value("${net.vipmro.kafka.bootstrap-servers}")
    private String bootstrapServers;
    /**默认消费者组*/
    @Value("${net.vipmro.kafka.consumer.group-id}")
    private String groupId;
    /**最早未被消费的offset*/
    @Value("${net.vipmro.kafka.consumer.auto-offset-reset}")
    private String autoOffsetReset;
    @Value("${net.vipmro.kafka.consumer.session-timeout-ms}")
    private Integer sessionTimeoutMs;
    @Value("${net.vipmro.kafka.consumer.request-timeout-ms}")
    private Integer requestTimeoutMs;
    /**批量一次最大拉取数据量*/
    @Value("${net.vipmro.kafka.consumer.max-poll-records}")
    private String maxPollRecords;
    /**批量一次拉去数据时间间隔*/
    @Value("${net.vipmro.kafka.consumer.poll-timeout-ms}")
    private Integer pollTimeoutMs;

    @Bean
    public Map<String, Object> consumerConfigs() {
        Map<String, Object> props = new HashMap<>();
        props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServers);
        props.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, autoOffsetReset);
        props.put(ConsumerConfig.GROUP_ID_CONFIG, groupId);
        props.put(ConsumerConfig.MAX_POLL_RECORDS_CONFIG, maxPollRecords);
        props.put(ConsumerConfig.SESSION_TIMEOUT_MS_CONFIG, sessionTimeoutMs);
        props.put(ConsumerConfig.REQUEST_TIMEOUT_MS_CONFIG, requestTimeoutMs);
        props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
        props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
        return props;
    }

    @Bean
    public ConsumerFactory<Integer, String> consumerFactory() {
        return new DefaultKafkaConsumerFactory<>(consumerConfigs());
    }

    @Bean
    KafkaListenerContainerFactory<ConcurrentMessageListenerContainer<Integer, String>> kafkaListenerContainerFactory() {
        ConcurrentKafkaListenerContainerFactory<Integer, String> factory = new ConcurrentKafkaListenerContainerFactory<>();
        factory.setConsumerFactory(consumerFactory());
        //并发数
        factory.setConcurrency(1);
        //是否批量消费
        factory.setBatchListener(true);
        //消费时间间隔
        factory.getContainerProperties().setPollTimeout(pollTimeoutMs);
        return factory;
    }

}
