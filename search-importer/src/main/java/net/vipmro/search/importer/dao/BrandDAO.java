package net.vipmro.search.importer.dao;


import net.vipmro.search.dto.entity.Brand;

import java.util.List;

/**
 * @author fengxiangyang
 */
public interface BrandDAO {

    List<Brand> findAll();

}
