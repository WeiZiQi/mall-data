package net.vipmro.search.importer.service.b2r;

import net.vipmro.search.dto.pojo.DataPacket;

/**
 * 全量更新索引数据
 * @author fengxiangyang
 * @date 2018/12/4
 */
public interface SellerGoodsInfoService {
    /**
     * 全量更新
     */
    void reindex();

    /**
     * 增量更新
     */
    void update(DataPacket dataPacket);
}
