package net.vipmro.search.dto.entity;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

import java.io.Serializable;

/**
 * 销售商品信息
 * @author fengxiangyang
 * @date 2018/12/4
 */
public class SellerGoodsInfo implements Serializable {
    private Long id;
    private String alias;
    private String batchQuantity;
    private Double beanRate;
    private Integer brandId;
    private String buyNo;
    private Integer categoryId;
    private String categoryNo;
    private Double convertNum;
    private String convertUnit;
    private Integer dealerId;
    private Integer deliveryTime;
    private Long goodsId;
    private String goodsName;
    private String goodsNo;
    private String image;
    private Integer isBean;
    private Integer isConvertShow;
    private String isFullcut;
    private Integer isFullgive;
    private Integer isGroupon;
    private Integer isHot;
    private Integer isRate;
    private String isRebates;
    private Integer isSale;
    private Integer isSupportReturn;
    private String keywords;
    private String marketPrice;
    private String measure;
    private String model;
    private String orderQuantity;
    private Integer orderSn;
    private String packageNum;
    private Double price;
    private Double rate;
    private Double salePrice;
    private Integer salesVolume;
    private Long sellerCateId;
    private Integer sellerId;
    private Double salesPriceVolume;
    private String secondCateId;
    private String series;
    private String spuId;
    private Integer stock;
    private String shopName;
    private String title;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getBatchQuantity() {
        return batchQuantity;
    }

    public void setBatchQuantity(String batchQuantity) {
        this.batchQuantity = batchQuantity;
    }

    public Double getBeanRate() {
        return beanRate;
    }

    public void setBeanRate(Double beanRate) {
        this.beanRate = beanRate;
    }

    public Integer getBrandId() {
        return brandId;
    }

    public void setBrandId(Integer brandId) {
        this.brandId = brandId;
    }

    public String getBuyNo() {
        return buyNo;
    }

    public void setBuyNo(String buyNo) {
        this.buyNo = buyNo;
    }

    public Integer getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategoryNo() {
        return categoryNo;
    }

    public void setCategoryNo(String categoryNo) {
        this.categoryNo = categoryNo;
    }

    public Double getConvertNum() {
        return convertNum;
    }

    public void setConvertNum(Double convertNum) {
        this.convertNum = convertNum;
    }

    public String getConvertUnit() {
        return convertUnit;
    }

    public void setConvertUnit(String convertUnit) {
        this.convertUnit = convertUnit;
    }

    public Integer getDealerId() {
        return dealerId;
    }

    public void setDealerId(Integer dealerId) {
        this.dealerId = dealerId;
    }

    public Integer getDeliveryTime() {
        return deliveryTime;
    }

    public void setDeliveryTime(Integer deliveryTime) {
        this.deliveryTime = deliveryTime;
    }

    public Long getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(Long goodsId) {
        this.goodsId = goodsId;
    }

    public String getGoodsName() {
        return goodsName;
    }

    public void setGoodsName(String goodsName) {
        this.goodsName = goodsName;
    }

    public String getGoodsNo() {
        return goodsNo;
    }

    public void setGoodsNo(String goodsNo) {
        this.goodsNo = goodsNo;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Integer getIsBean() {
        return isBean;
    }

    public void setIsBean(Integer isBean) {
        this.isBean = isBean;
    }

    public Integer getIsConvertShow() {
        return isConvertShow;
    }

    public void setIsConvertShow(Integer isConvertShow) {
        this.isConvertShow = isConvertShow;
    }

    public String getIsFullcut() {
        return isFullcut;
    }

    public void setIsFullcut(String isFullcut) {
        this.isFullcut = isFullcut;
    }

    public Integer getIsFullgive() {
        return isFullgive;
    }

    public void setIsFullgive(Integer isFullgive) {
        this.isFullgive = isFullgive;
    }

    public Integer getIsGroupon() {
        return isGroupon;
    }

    public void setIsGroupon(Integer isGroupon) {
        this.isGroupon = isGroupon;
    }

    public Integer getIsHot() {
        return isHot;
    }

    public void setIsHot(Integer isHot) {
        this.isHot = isHot;
    }

    public Integer getIsRate() {
        return isRate;
    }

    public void setIsRate(Integer isRate) {
        this.isRate = isRate;
    }

    public String getIsRebates() {
        return isRebates;
    }

    public void setIsRebates(String isRebates) {
        this.isRebates = isRebates;
    }

    public Integer getIsSale() {
        return isSale;
    }

    public void setIsSale(Integer isSale) {
        this.isSale = isSale;
    }

    public Integer getIsSupportReturn() {
        return isSupportReturn;
    }

    public void setIsSupportReturn(Integer isSupportReturn) {
        this.isSupportReturn = isSupportReturn;
    }

    public String getKeywords() {
        return keywords;
    }

    public void setKeywords(String keywords) {
        this.keywords = keywords;
    }

    public String getMarketPrice() {
        return marketPrice;
    }

    public void setMarketPrice(String marketPrice) {
        this.marketPrice = marketPrice;
    }

    public String getMeasure() {
        return measure;
    }

    public void setMeasure(String measure) {
        this.measure = measure;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getOrderQuantity() {
        return orderQuantity;
    }

    public void setOrderQuantity(String orderQuantity) {
        this.orderQuantity = orderQuantity;
    }

    public Integer getOrderSn() {
        return orderSn;
    }

    public void setOrderSn(Integer orderSn) {
        this.orderSn = orderSn;
    }

    public String getPackageNum() {
        return packageNum;
    }

    public void setPackageNum(String packageNum) {
        this.packageNum = packageNum;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Double getRate() {
        return rate;
    }

    public void setRate(Double rate) {
        this.rate = rate;
    }

    public Double getSalePrice() {
        return salePrice;
    }

    public void setSalePrice(Double salePrice) {
        this.salePrice = salePrice;
    }

    public Integer getSalesVolume() {
        return salesVolume;
    }

    public void setSalesVolume(Integer salesVolume) {
        this.salesVolume = salesVolume;
    }

    public Long getSellerCateId() {
        return sellerCateId;
    }

    public void setSellerCateId(Long sellerCateId) {
        this.sellerCateId = sellerCateId;
    }

    public Integer getSellerId() {
        return sellerId;
    }

    public void setSellerId(Integer sellerId) {
        this.sellerId = sellerId;
    }

    public Double getSalesPriceVolume() {
        return salesPriceVolume;
    }

    public void setSalesPriceVolume(Double salesPriceVolume) {
        this.salesPriceVolume = salesPriceVolume;
    }

    public String getSecondCateId() {
        return secondCateId;
    }

    public void setSecondCateId(String secondCateId) {
        this.secondCateId = secondCateId;
    }

    public String getSeries() {
        return series;
    }

    public void setSeries(String series) {
        this.series = series;
    }

    public String getSpuId() {
        return spuId;
    }

    public void setSpuId(String spuId) {
        this.spuId = spuId;
    }

    public Integer getStock() {
        return stock;
    }

    public void setStock(Integer stock) {
        this.stock = stock;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public String toString() {
        return ReflectionToStringBuilder.toString(this);
    }
}
