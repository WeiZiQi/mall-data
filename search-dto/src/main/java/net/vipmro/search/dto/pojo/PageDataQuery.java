package net.vipmro.search.dto.pojo;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

public class PageDataQuery {
    /**
     * 开始ID
     */
    private Long id;
    /**
     * 每页的数量
     */
    private Integer pageSize;

    public PageDataQuery() {
    }

    public PageDataQuery(Long id, Integer pageSize) {
        this.id = id;
        this.pageSize = pageSize;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    @Override
    public String toString() {
        return ReflectionToStringBuilder.toString(this);
    }
}
