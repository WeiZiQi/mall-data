package net.vipmro.search.dto.constant;

/**
 * @author fengxiangyang
 * @date 2018/12/3
 */
public enum BusinessCacheKey {
    SELLER_GOODS_INFO,
    GOODS_ATTR,
    BRAND,
    CATEGORY,
    CATEGORY_ATTR
    ;
    private final String value;

    BusinessCacheKey() {
        this.value = "SEARCH:BUSINESS:" + name();
    }

    public String key() {
        return value;
    }

    public String key(Object... params) {
        StringBuilder key = new StringBuilder(value);
        if (params != null && params.length > 0) {
            for (Object param : params) {
                key.append(':');
                key.append(String.valueOf(param));
            }
        }
        return key.toString();
    }
}
