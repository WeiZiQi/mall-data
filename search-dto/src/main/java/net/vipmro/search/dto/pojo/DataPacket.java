package net.vipmro.search.dto.pojo;

import java.util.List;

public class DataPacket {
    private Long txId;
    private List<TableData> dataList;

    public Long getTxId() {
        return txId;
    }

    public void setTxId(Long txId) {
        this.txId = txId;
    }

    public List<TableData> getDataList() {
        return dataList;
    }

    public void setDataList(List<TableData> dataList) {
        this.dataList = dataList;
    }
}
